package com.vect0r.jsonparser;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class WorkersActivity extends AppCompatActivity implements View.OnClickListener {
    public ArrayList workers = new ArrayList();
    //public MainActivity.Specialties specialties = null;
    public static String LOG_TAG = "jsonparselog";
    TextView textview;
    Intent intent = getIntent();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        intent = getIntent();
        //workers = (ArrayList) intent.getSerializableExtra("workers");
        //specialties = (MainActivity.Specialties) intent.getSerializableExtra("specialties");
        //showWorkers(workers); //покажем всех работников
    }

    @Override
    public void onClick(View v) { //если щелкнули на работнике - покажем активити с детальной информацией о нем. ИД работника = ИД view'шки
        Intent intent = new Intent(WorkersActivity.this, WorkerActivity.class);
        //intent.putExtra("worker", (MainActivity.Worker) workers.get(v.getId()));
        //intent.putExtra("specialties", specialties);
        startActivity(intent);
    }
/*
    public void showWorkers(ArrayList workers) {
        TableLayout tablelayout = (TableLayout) findViewById(R.id.table_layout_wrkrs);
        TableRow rowNew;
        TextView newText;
        ImageView imgView;
        TableRow.LayoutParams lParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lParams.bottomMargin = 16;
        lParams.topMargin = 16;
        lParams.leftMargin = 16;
        lParams.rightMargin = 16;
        lParams.gravity = Gravity.CENTER_VERTICAL;
        //шапка
        rowNew = new TableRow(WorkersActivity.this);
        newText = new TextView(WorkersActivity.this);
        newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
        newText.setText("Фамилия");
        rowNew.addView(newText, lParams);
        newText = new TextView(WorkersActivity.this);
        newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
        newText.setText("Имя");
        rowNew.addView(newText, lParams);
        newText = new TextView(WorkersActivity.this);
        newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
        newText.setText("Возраст");
        rowNew.addView(newText, lParams);
        newText = new TextView(WorkersActivity.this);
        newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
        newText.setText("Фото");
        rowNew.addView(newText, lParams);
        tablelayout.addView(rowNew);
        //работники
        for (int i = 0; i < workers.size(); i++){
            MainActivity.Worker worker = (MainActivity.Worker) workers.get(i);
            rowNew = new TableRow(WorkersActivity.this);
            rowNew.setId(i);
            newText = new TextView(WorkersActivity.this);
            newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
            newText.setText(worker.l_name);
            rowNew.addView(newText, lParams);
            newText = new TextView(WorkersActivity.this);
            newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
            newText.setText(worker.f_name);
            rowNew.addView(newText, lParams);
            newText = new TextView(WorkersActivity.this);
            newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
            if (worker.age !=null) {
                newText.setText(worker.age);
            } else {
                newText.setText("-");
            }
            rowNew.addView(newText, lParams);
            imgView = new ImageView(WorkersActivity.this);

            imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imgView.setBackgroundColor(Color.BLACK);
            TableRow.LayoutParams lParamsImg = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lParamsImg.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, getResources().getDisplayMetrics());
            lParamsImg.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, getResources().getDisplayMetrics());
            rowNew.addView(imgView, lParamsImg);
            try {
                Picasso.with(this).load(worker.avatar_url).fit().centerCrop().into(imgView);
            } catch (IllegalArgumentException e) { //если picasso не переварил фото по url или сам url - обработаем...
                Log.d(LOG_TAG, "Path " + worker.avatar_url + " is not valid");
                e.printStackTrace();
            }
            rowNew.setOnClickListener(this);
            tablelayout.addView(rowNew);

        }
    }
    */
}
