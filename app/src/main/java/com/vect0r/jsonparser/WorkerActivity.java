package com.vect0r.jsonparser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;

public class WorkerActivity extends AppCompatActivity {
    //public MainActivity.Worker worker = null;
    //public MainActivity.Specialties specialties = null;
    public static String LOG_TAG = "jsonparselog";
    Intent intent = getIntent();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        intent = getIntent();
        //worker = (MainActivity.Worker) intent.getSerializableExtra("worker");
        //specialties = (MainActivity.Specialties) intent.getSerializableExtra("specialties");
        //showWorker(worker,specialties); //покажем данные о работнике
    }

    //метод отрисовывает на экран работника
    /*
    private void showWorker(MainActivity.Worker worker, MainActivity.Specialties specialties) {
        LinearLayout linearlayout = (LinearLayout) findViewById(R.id.linear_layout_wrkr);
        ImageView imgView = (ImageView) findViewById(R.id.imageView);
        try {
            Picasso.with(this).load(worker.avatar_url).resizeDimen(R.dimen.img_large_width, R.dimen.img_large_height).centerCrop().into(imgView);
        } catch (IllegalArgumentException e) {
            Log.d(LOG_TAG, "Path " + worker.avatar_url + " is not valid");
            e.printStackTrace();
        }
        TextView textView = (TextView) findViewById(R.id.workerFName);
        textView.setText(worker.f_name); //имя
        textView = (TextView) findViewById(R.id.workerLName);
        textView.setText(worker.l_name); //фамилия
        textView = (TextView) findViewById(R.id.workerBirthDate);
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        if (worker.birthdate != null) { //если есть дата рождения - показываем
            textView.setText(formatter.format(worker.birthdate));
        } else {
            textView.setText("-");
        }
        textView = (TextView) findViewById(R.id.workerAge);
        if (worker.age != null) { //если есть данные о возрасте - кажем
            textView.setText(worker.age);
        } else {
            textView.setText("-");
        }
        textView = (TextView) findViewById(R.id.workerSpecialty); //кажем специальности
        String profs = "";
        for(int i = 0; i < worker.specialties.size(); i++) { //если специальностей несколько, покажем все через запятую
            profs += ", " + specialties.getById((int) worker.specialties.get(i)).toLowerCase();
        }
        textView.setText(profs.substring(2,3).toUpperCase() + profs.substring(3));
    }
    */
}
