package com.vect0r.jsonparser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements Serializable, View.OnClickListener{

    public static String LOG_TAG = "jsonparselog"; //тэг для логката
    private static String URL="http://65apps.com/images/testTask.json"; //URL JSON-файла
    private String jsonString;
    public Workers workers; //объект для всех работников
    //public Specialties specialties; //объект для всех специальностей

    @Override
    public void onClick(View v) {
        Log.d(LOG_TAG, Integer.toString(v.getId()));
        Intent intent = new Intent(MainActivity.this, WorkersActivity.class);
        //intent.putExtra("workers", workers.GetWorkersByProfId(v.getId()));  //передаем в дочерний activitу работников..
        //intent.putExtra("specialties", specialties);                        //..и специальности
        startActivity(intent);
    }

    class Specialty implements Serializable{
        private int specialty_id = 0;
        private String name = "";
    }

    /*
    //класс для работы со списком всех специальностей, методы getById, getAll
    class Specialties implements Serializable {
        List<Specialty> specialties = new LinkedList<>();
        private ArrayList specs = new ArrayList();
        public void add(int input_id, String input_name) {
            Specialty spec = new Specialty();
            spec.id = input_id;
            spec.name = input_name;
            specs.add(spec);
         }
        //метод возвращает наименование специальности по ИД
        public String getById (int input_id) {
            Specialty spec = new Specialty();
            for (int i = 0; i < specs.size(); i++){
                spec = (Specialty) specs.get(i);
                if (spec.id == input_id) return spec.name;
            }
            return null;
        }
        //метод возвращает все специальности в виде ArrayList, состоящего из объектов класса Specialty
        public ArrayList getAll (){
            return specs;
        }

    }
    */
    //класс, описывающий работника.
    class Worker implements Serializable{
        public String f_name = "";                      //имя
        public String l_name = "";                      //фамилия
        public String birthday = "";                   //дата рождения
        public String avatr_url = "";
        //public String age = null;                       //возраст в текстовом виде
        public List<Specialty> specialty = new LinkedList(); //ArrayList из объектов Specialty
    }

    //класс для работы со всеми работниками. Методы: GetWorkersByProfId
    class Workers implements Serializable{
        List<Worker> response = new LinkedList<>();
        /*
        private ArrayList workers = new ArrayList();
        public void add(String fname, String lname, Date in_birthdate, String in_age, String av_url, ArrayList specs) {
            Worker new_worker = new Worker();
            new_worker.f_name = fname;
            new_worker.l_name = lname;
            new_worker.birthdate = in_birthdate;
            new_worker.age = in_age;
            new_worker.avatar_url = av_url;
            new_worker.specialties = specs;
            workers.add(new_worker);
        }
        //метод возвращает ArrayList из Worker по ИД специальности, которая у них есть
        public ArrayList GetWorkersByProfId(int index){
            ArrayList workersArray = new ArrayList();
            Worker worker = new Worker();
            for (int i = 0; i < workers.size(); i++){
                worker = (Worker) workers.get(i);
                ArrayList specs = worker.specialties;
                for (int j = 0; j < specs.size(); j++) {
                    if ((int) specs.get(j) == index) workersArray.add(worker);
                }
            }
            return workersArray;
        }*/

    }

    //класс-поток, реализующий вытаскивание файла json из сети
    private class getJsonFromNetThread extends Thread {
        @Override
        public void run() {
            synchronized (this) {
                try {  //сначала скачаем json на устройство
                    jsonString = downloadFile(URL);     //не слишком хорошо в String, но здесь - покатит...
                    notify();
                } catch (IOException e) {
                    Log.d(LOG_TAG, "Нет подключения.");
                    //если подключения нету - покажем toast ругающийся...
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast toast = Toast.makeText(MainActivity.this, "Нет подключения", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    });
                    notify();
                } catch (Exception e) {
                    Log.d(LOG_TAG, "Json не получен.");
                    e.printStackTrace();
                    notify();
                }
            }
        }
    }

    private String downloadFile(String url) throws IOException {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        File cacheDir = MainActivity.this.getCacheDir();
        Cache cache = new Cache(cacheDir, cacheSize);
        OkHttpClient client = new OkHttpClient.Builder()
                .cache(cache)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainactivity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getJsonData(); //забираем Json
        this.parseJsonData(); //парсим Json по объектам предметным
        //this.showSpecialities(); //показываем список специальностей на экран
    }

    //метод для запуска класса-потока, вытягивающего Json из сети в файл на устройстве
    private void getJsonData () {
        getJsonFromNetThread getJsonThread = new getJsonFromNetThread();
        getJsonThread.start();
        synchronized (getJsonThread){
            try {
                Log.d(LOG_TAG, "Ждем заливки json...");
                getJsonThread.wait();
            } catch (InterruptedException e) {
                Log.d(LOG_TAG, "Json не залился!");
                e.printStackTrace();
            }
        }
    }

    //метод читает файл кэшированного json на устройстве и формирует объекты классов Specialties (список специальностей) и Workers (работники)
    private void parseJsonData() {
        try {

            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            workers = gson.fromJson(jsonString, Workers.class);
            Log.d(LOG_TAG, "Работников получено: " + workers.response.get(0).avatr_url);
            /*for (int i = 0; i < response.length(); i++) {
                JSONObject worker_item = response.getJSONObject(i);
                String f_name = worker_item.getString("f_name").toLowerCase(); //имя в нижний регистр...
                f_name = f_name.substring(0,1).toUpperCase()+f_name.substring(1, f_name.length()); //капитализируем первую букву
                String l_name = worker_item.getString("l_name").toLowerCase(); //фамилия в нижний регистр...
                l_name = l_name.substring(0,1).toUpperCase()+l_name.substring(1, l_name.length()); //капитализируем первую букву
                String avatr_url = worker_item.getString("avatr_url"); //забираем url аватара, какой бы он ни был...
                JSONArray specialty = worker_item.getJSONArray("specialty"); //берем массив специальностей
                ArrayList worker_specs = new ArrayList();
                for (int j = 0; j < specialty.length(); j++) { //складываем массив специальностей в Specialties, соблюдая уникальность ИД
                    JSONObject specialty_item = specialty.getJSONObject(j);
                    int spec_id = specialty_item.getInt("specialty_id");
                    String spec_name = specialty_item.getString("name");
                    if (specialties.getById(spec_id) == null) specialties.add(spec_id, spec_name); //если с таким ИД еще нету - добавляем
                    worker_specs.add(spec_id);
                }
                String birthday = worker_item.getString("birthday");//парсим текст с датой
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date birthDate = null;
                String ymdPattern = "\\d{4}-\\d{2}-\\d{2}";
                String dmyPattern = "\\d{2}-\\d{2}-\\d{4}";
                if (birthday.matches(ymdPattern)) {
                    formatter = new SimpleDateFormat("yyyy-MM-dd");
                }
                if (birthday.matches(dmyPattern)) {
                    formatter = new SimpleDateFormat("dd-MM-yyyy");
                }
                try {
                    birthDate = formatter.parse(birthday);
                } catch (ParseException e) { //если не удалось - то увы...
                    Log.d(LOG_TAG, "Неподдерживаемый формат даты: '" + birthday + "'");
                    e.printStackTrace();
                }
                //формируем текст с возрастом в понятном виде
                String strAge = null; //строка с цифрами возраста
                if (birthDate != null) {
                    Date currentDate = new Date();
                    int age = currentDate.getYear() - birthDate.getYear();
                    int month = currentDate.getMonth() - birthDate.getMonth();
                    if (month < 0 || month == 0 &&  currentDate.getDay() < birthDate.getDay()) {
                        age--;
                    }
                    String ageLabel = ""; //подпись ...лет, ... года и тд
                    if (age > 9 && age < 20) {
                        ageLabel = " лет";
                    } else {
                        switch (Integer.decode(Integer.toString(age).substring(Integer.toString(age).length()-1,Integer.toString(age).length()))) {
                            case 1:
                                ageLabel = " год";
                                break;
                            case 2:case 3:case 4:
                                ageLabel = " года";
                                break;
                            default:
                                ageLabel = " лет";
                        }
                    }
                    strAge = Integer.toString(age) + ageLabel; //цифры + подпись
                }
                workers.add(f_name, l_name, birthDate, strAge, avatr_url, worker_specs); //сформировали все переменные - создали работника
            }
            */
        } catch (Exception e) {
            Log.d(LOG_TAG, "Получен невалидный json: "  + jsonString);
            e.printStackTrace();
        }
    }
    //метод отображает на экран список специальностей

/*
    private void showSpecialities() {
        TableLayout tablelayout = (TableLayout) findViewById(R.id.table_layout);
        tablelayout.removeAllViews();
        TableRow rowNew;
        TextView newText;
        TableRow.LayoutParams lParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lParams.bottomMargin = 16;
        lParams.topMargin = 16;
        lParams.leftMargin = 16;
        lParams.rightMargin = 16;
        rowNew = new TableRow(this);
        newText = new TextView(this);
        newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
        newText.setText("Код");
        rowNew.addView(newText, lParams);
        newText = new TextView(this);
        newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
        newText.setText("Наименование");
        rowNew.addView(newText, lParams);
        tablelayout.addView(rowNew);
        ArrayList spec_list = specialties.getAll(); //получаем все специальности и выводим на экран в цикле...
        for (int i = 0; i < spec_list.size(); i++){
            Specialty spec = (Specialty) spec_list.get(i);
            rowNew = new TableRow(this);
            rowNew.setId(spec.id);
            newText = new TextView(this);
            newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
            newText.setText(Integer.toString(spec.id));
            rowNew.addView(newText, lParams);
            newText = new TextView(this);
            newText.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Medium);
            newText.setText(spec.name);
            rowNew.addView(newText, lParams);
            rowNew.setOnClickListener(this);
            tablelayout.addView(rowNew);
        }
    }
*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) { //если жмакнули "обновить"...
            this.getJsonData(); //забираем Json
            this.parseJsonData(); //парсим Json по объектам предметным
            //this.showSpecialities(); //показываем список специальностей на экран
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}